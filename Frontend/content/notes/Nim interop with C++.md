+++
title = "Nim interop with C++"
date =2024-04-12  
description = "Nim's support for interop with C++ is next to none"
draft=true

[taxonomies]
tags = ["Nim", "C++"]
+++

# Todo
- add glossory for links and github repo



---

Recently stumbled upon Nim which claims to have the *simplicity of Python, speed of C and Extensibility of Lisp.* After writing some code in it for a while I can somewhat agree with claims.

To add to that the interoperability of Nim with C and even **C++** is next to none, truly a great language ❤️.

## With C

I won't be diving deep into the syntax of the language as I covered it in another [blog](@/posts/nim1.md).
## C files with declarations 

```c
#include <stdio.h>

void print_hello(void) { printf("Hello World\n"); }

```

we can call this function in Nim using 

```nim
{.compile: "main.c".}

proc print_hello() {.importc: "print_hello", nodecl.}
print_hello()

```

- We use the `.compile` pragma to compile our Nim file with `main.c` where the function definition is 
- Use `importc` to pick which C function should be called when `print_hello` is called
## C constants

Assume you have a C constant that you want to use in Nim
```c
#define num 10
```

there is no direct and clean way to call a C constant but we have a workaround
```nim
var x {.importc: "num", header: "main.c", nodecl.}: cint
```

- We use the `.importc` pragma on a variable `x` of type `cint` and access the `num` variable 
## C object files

assume there is some function like 
```c
// add.h
int add(int a, int b);
```

and it's implementation is inside an object file
then we can use it in Nim as follows

```nim
{.link: "main.o".}

proc add(a:cint, b:cint):cint {.importc: "add", nodecl.}

```


## Arguement passing 

1. Using `@`: When you prefix an argument with `@`, it passes all arguments provided to the Nim function directly to the corresponding C/C++ function. This is useful when you want to pass all arguments without modification and maintain the same function signature.
    
2. Using `#`: Prefixing an argument with `#` allows you to access individual arguments sequentially. Each subsequent `#` accesses the next argument in order. This is helpful when you only need to pass certain arguments or modify them before passing them to the C/C++ function.




