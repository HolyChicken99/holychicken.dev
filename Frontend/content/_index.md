+++
title = "TCombinator"
description = " a  pan-dimensional blog series for hyper-intelligent species of beings "
+++

**TCombinator** a  pan-dimensional blog series for, hyper-intelligent species of beings (whose three-dimensional protrusions into our universe are dudes sitting on computer). Join our august coterie of Tanimbar-inspired coders as we embark on an odyssey through the ineffable frontiers of compilers, niche-languages and such. 

| Symbol | Bird     | Combinator |
|--------|----------|------------|
| S      | Starling | S          |
| B      | Bluebird | S(KS)K     |
| E      | Eagle    | B(BBB)     |
| T      | Tanimbar | T          |
| J      | Jay  | B(BC)(W(BC(B(BBB))))        |

<style>
center{
  font-weight: bold;
  text-align: center;
}
</style>


<center styles="font-weight: bold;">Tanimbar is the TCombinator</center>

