+++
title = "Tame the dragon:2"
date = 2023-05-11
description = "Generating code"
draft = true

[taxonomies]
tags = ["Elixir", "Compilers", "LLVM"]
+++

Fairy tales are more than real not because they tell us dragon exists but because they tell us that they can be **defeated** and the dragon we will be defeating is **Code Generation** for our compiler.

<!-- more -->

# Table of contents
1. [Introduction](#Introduction)

{{ image(src="https://i.ibb.co/s206lz6/1833297.jpg",
      style="width: 50%;height: 50%;",
         position="center") }}

# Introduction

Last blog we covered lexical analysis of tokens and how compilers give meaning to sequences of character. The next phase in a compiler is parsing which is to identify the relation of words with each other, doesn't make sense? Read this blog 

{{ image(src="/images/CompilerPhases.png",
      style="width: 50%;height: 50%;",
         position="center") }}


Now that we know what Parsers do lets start by defining our languages Grammar rules. 
These rules will define how expressions must be formed in our language

```
<Expr> ::= <
```

