#+title: Devlogs

* <2023-03-22 Wed>
** use windows 98 css
** TODO first series: tame the dragon: conquering Compilers


* <2023-03-23 Thu>
** Not using svelte but regular html/css
** svelte -> too difficult to generate multiple htmls as targets
** Database for the blogs
** TODO Start with taming the dragon

* [2023-04-05 Wed]
** TODO Make a markdown to html parser for this ?
** About page first iteration almost done
** start with integrating ~slurp~ with the static files
** add way to syntax highlight languages
** tag based searching

*  <2023-04-13 Thu>
** TODO Make css preloader animations
** Changed name of the blogsite from ~Holychicken.dev~ to ~T-Combinator~
** TODO Make a button (ADHD helper on top), bolds first 2 letters of every word

** [2023-05-11 Thu]
***** Using zola with premade themes
***** Zola uses its own serve commands to might delete slurp
* <2023-07-16 Sun>
** Updated Marsec and gave it a proof read
*** Added grammar examples and am covering ambiguity in CFG
*** TODO Go over Regex vs CFG in detail
